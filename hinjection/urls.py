from django.contrib import admin
from django.contrib.auth.decorators import login_required
from django.conf.urls import patterns, include, url
from django.views.generic import RedirectView

from books import views as book_views

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', RedirectView.as_view(url='/items/')),

    url(r'^items/?$', login_required(book_views.get_books), name='get_books'),
    url(r'^items/(?P<book_id>\w{0,50})/$', login_required(book_views.get_book), name='get_book'),
    url(r'^items/(?P<book_id>\w{0,50})/(?P<chapter_id>\w{0,50})$',
        login_required(book_views.get_chapter), name='get_chapter'),

    url(r'^admin/books/book/add/?$', login_required(book_views.BookUploadView.as_view()), name='upl'),

    url(r'^accounts/login/$', 'django.contrib.auth.views.login', {'template_name': 'accounts/login.html'}),
    url(r'^accounts/logout/$', 'django.contrib.auth.views.logout_then_login'),

    url(r'^admin/', include(admin.site.urls)),
)
