from django.db import models


class Book(models.Model):
    title = models.CharField(max_length=255)
    author = models.CharField(max_length=255)


class ChapterManager(models.Manager):

    def get_previous(self, chapter):
        if chapter.index_number == 1:
            return None
        return self._get_with_offset(chapter, -1)

    def get_next(self, chapter):
        return self._get_with_offset(chapter, 1)

    def _get_with_offset(self, chapter, offset):
        try:
            return self.get(book=chapter.book, index_number=chapter.index_number + offset)
        except Chapter.DoesNotExist:
            return None

class Chapter(models.Model):
    title = models.CharField(max_length=255)
    content = models.TextField()
    book = models.ForeignKey(Book)
    index_number = models.IntegerField()

    objects = ChapterManager()
