from django.conf import settings
from django.http import Http404, HttpResponseRedirect
from django.shortcuts import render
from django.views.generic import FormView

from books import upload
from books.forms import BookUploadForm
from books.models import Book, Chapter


class BookUploadView(FormView):
    success_url = '/admin/books/book'
    template_name = 'upload_book.html'
    form_class = BookUploadForm

    def form_valid(self, form):
        try:
            upload.process_url(form.cleaned_data['url'])
            return HttpResponseRedirect(self.get_success_url())
        except upload.UploadError:
            form.errors['url'] = ['wrong fb2(archive)']
            return render(self.request, self.template_name, {'form': form})


def get_books(request):
    return render(request, 'books.html', {'books': Book.objects.all()})


def get_book(request, book_id):
    try:
        book = Book.objects.get(id=book_id)
    except Exception:
        raise Http404
    return render(request, 'book.html', {'book': book})


def get_chapter(request, book_id, chapter_id):
    try:
        book = Book.objects.get(id=book_id)
        chapter = Chapter.objects.get(id=chapter_id)
    except Exception:
        raise Http404
    spritz_redirect_url = request.build_absolute_uri('{}{}'.format(settings.STATIC_URL, 'login_success.html'))
    previous_chapter = Chapter.objects.get_previous(chapter)
    next_chapter = Chapter.objects.get_next(chapter)
    return render(request, 'chapter.html', {'book': book, 'chapter': chapter, 'next_chapter': next_chapter,
                                            'previous_chapter': previous_chapter,
                                            'spritz_redirect_url': spritz_redirect_url})
