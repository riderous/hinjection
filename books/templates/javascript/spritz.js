var SpritzSettings = {
   clientId: "3349145a4bd38adf0",
   redirectUri: "{{ spritz_redirect_url }}"
};

var spritzController = null;

var onSpritzifySuccess = function(spritzText) {
    spritzController.startSpritzing(spritzText);
};

var onSpritzifyError = function(error) {
    alert("Unable to Spritz: " + error.message);
};

// Customized options
var customOptions = {
    "redicleWidth" : 	320,	// Specify Redicle width
    "redicleHeight" : 	76		// Specify Redicle height
};

function onStartSpritzClick(event) {
    var text = $('#inputText').val().slice($('#inputText').caret());
    var locale = "en_us;";

    // Send to SpritzEngine to translate
    SpritzClient.spritzify(text, locale, onSpritzifySuccess, onSpritzifyError);
};


var init = function() {
    $("#startSpritz").on("click", onStartSpritzClick);

    // Construct a SpritzController passing the customization options
    spritzController = new SPRITZ.spritzinc.SpritzerController(customOptions);

    // Attach the controller's container to this page's "spritzer" container
    spritzController.attach($("#spritzer"));
};

$(document).ready(function() {
    init();
});
