import os

from lxml import etree


class Fb2Parser(object):
    NS = {"t": "http://www.gribuser.ru/xml/fictionbook/2.0"}
    DESCRIPTION_XPATH = 't:description/t:title-info'
    XSL_PATH = os.path.join(os.path.dirname(__file__), 'FB2_2_txt.xsl')

    def __init__(self, fb2_file):
        self.doc = etree.fromstring(fb2_file)
        self.xslt_transform = etree.XSLT(etree.parse(self.XSL_PATH).getroot())

    def parse_title(self):
        return self.doc.xpath('{}/t:book-title/text()'.format(self.DESCRIPTION_XPATH), namespaces=self.NS)[0]

    def parse_author(self):
        fns = self.doc.xpath('{}/t:author/t:first-name/text()'.format(self.DESCRIPTION_XPATH), namespaces=self.NS)
        mns = self.doc.xpath('{}/t:author/t:middle-name/text()'.format(self.DESCRIPTION_XPATH), namespaces=self.NS)
        lns = self.doc.xpath('{}/t:author/t:last-name/text()'.format(self.DESCRIPTION_XPATH), namespaces=self.NS)
        nns = self.doc.xpath('{}/t:author/t:nickname/text()'.format(self.DESCRIPTION_XPATH), namespaces=self.NS)

        #Assume up to 10 authors
        authors = []
        for i in xrange(10):
            author = []
            if len(fns) > i:
                author.append(fns[i])
            if len(mns) > i:
                author.append(mns[i])
            if len(lns) > i:
                author.append(lns[i])
            if len(nns) > i:
                author.append('({})'.format(nns[i + 1]))
            if author:
                authors.append(' '.join(author))
            else:
                break
        return ', '.join(authors) if authors else 'unknown'

    def parse_content(self):
        for section in self.doc.xpath('t:body[not(@name)]/t:section', namespaces=self.NS):
            title = section.xpath('t:title/t:p/text()', namespaces=self.NS)
            yield title[0] if title else None, self.xslt_transform(etree.XML(etree.tostring(section)))
