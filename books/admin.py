from django.contrib import admin

from books.models import Book, Chapter


class ChapterInline(admin.TabularInline):
    model = Chapter


class BookAdmin(admin.ModelAdmin):
    list_display = ['id', 'title', 'author']

    inlines = [
        ChapterInline,
    ]

admin.site.register(Book, BookAdmin)
