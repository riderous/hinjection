from django import forms


class BookUploadForm(forms.Form):
    url = forms.URLField(required=True)
