import requests
import zipfile

from StringIO import StringIO

from books.fb2 import Fb2Parser
from books.models import Book, Chapter


class UploadError(Exception):
    pass


def process_url(url):
    try:
        parser = Fb2Parser(handle_file(url))
        save_book(parser.parse_title(), parser.parse_author(), parser.parse_content())
    except Exception as why:
        raise UploadError(why)


def handle_file(url):
    r = requests.get(url, stream=True)
    f = StringIO()
    for chunk in r.iter_content(chunk_size=1024):
        if chunk:
            f.write(chunk)
            f.flush()

    if zipfile.is_zipfile(f):
        zip_file = zipfile.ZipFile(f)

        #Assume it's only one file in the archive
        f = zip_file.read(zip_file.namelist()[0])
    return f


def save_book(title, author, chapters):
    book = Book(title=title, author=author)
    book.save()

    for i, (chapter_title, chapter_content) in enumerate(chapters):

        #TODO: make chapters hierarchical
        if str(chapter_content).strip():
            chapter_index_number = i + 1
            chapter = Chapter(title=chapter_title or str(chapter_index_number), content=chapter_content,
                              book=book, index_number=chapter_index_number)
            chapter.save()
